﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spur
{
    class SpurFile
    {
        public string filePath { get; set; }
        public string fileName { get; set; }
        public string supplierType { get; set; }
        public string supplierName { get; set; }
        public string orderCost { get; set; }
        
        public SpurFile(string filePath)
        {
            this.filePath = filePath;
        }
    }
}
