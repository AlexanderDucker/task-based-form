﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spur
{
    class SupplierInfo
    {
        //List<String> supplierName = new List<String>();
        //List<String> supplierType = new List<String>();

        public List<String> supplierName { get; set; }
        public List<String> supplierType { get; set; }

        public SupplierInfo()
        {
            supplierName = new List<String>();
            supplierType = new List<String>();
        }

    }
}
