﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using System.Collections.Concurrent;
using System.Threading;
namespace Spur
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        Worker _worker = new Worker();
        bool loaded = false;
        bool storeLoaded = false;
        bool complete = false;
        FileInfo file;
        List<StoreCodes> storeCodes = new List<StoreCodes>();
        ConcurrentBag<List<SpurFile>> overallBag = new ConcurrentBag<List<SpurFile>>();
        SupplierInfo supInfo = new SupplierInfo();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void FileBrowseClick(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".csv";
            dlg.Filter = "CSV Files (*.csv)|*.csv";
            string filename;


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                filename = dlg.FileName;
                FileBox.Text = filename;
                file = new FileInfo(filename);
                LoadButton.IsEnabled = true;
                storeLoaded = false;
                
            }
        }

        private void LoadClick(object sender, RoutedEventArgs e)
        {
            if(FileBox.Text.Equals("Click Browse to select Store Codes file"))
            {
                return;
            }

            else
            {
                if(!storeLoaded)
                {
                    storeLoaded = true;
                    //List<StoreCodes> storeCodes = new List<StoreCodes>();
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    storeCodes = _worker.LoadStoreCodes(file);
                    sw.Stop();
                    FileBox.Text = "File Loaded";
                    storeComboBox.ItemsSource = _worker.PopulateStoreBox(storeCodes);
                    weekComboBox.ItemsSource = _worker.PopulateWeekBox();
                    storeComboBox.SelectedIndex = 0;
                    weekComboBox.SelectedIndex = 0;
                    storeComboBox.IsEnabled = true;
                    weekComboBox.IsEnabled = true;
                    folderBrowse.IsEnabled = true;
                    folderLoad.IsEnabled = true;
                    LoadButton.IsEnabled = false;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Create instance of FolderBrowserDialog
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            //Display folder path in textbox
            string filename = dialog.SelectedPath;
            BrowseBox.Text = filename;
            loaded = false;
        }

        private void CalculateClick(object sender, RoutedEventArgs e)
        {
            string supplierName = supplierNameBox.Text;
            string supplierType = supplierTypeBox.Text;
            GridInfo gridInfo = _worker.CheckOrderCount(overallBag, supplierName, supplierType);
            UpdateGrid(gridInfo);
            costLabel.Content = gridInfo.cost;
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            if (BrowseBox.Text.Equals("Click Browse to select a folder"))
            {
                return;
            }
                
            else
            {
                if (!loaded)
                {
                    loaded = true;
                    String path = BrowseBox.Text;
                    string[] fileArray = Directory.GetFiles(@path, "*.csv", SearchOption.AllDirectories);
                    string storeQuery = storeComboBox.Text;
                    string weekQuery = weekComboBox.Text;
                    ConcurrentBag<SpurFile> fileNames = new ConcurrentBag<SpurFile>();
                    Stopwatch s = new Stopwatch();
                    s.Start();
                    if(storeQuery != "All")
                    {
                        foreach (StoreCodes sc in storeCodes)
                        {
                            if (sc.storeName == storeQuery)
                            {
                                storeQuery = sc.storeCode;
                                break;
                            }
                        }
                    }
                    string overallQuery = SearchQuery(storeQuery, weekQuery);
                    Parallel.ForEach(fileArray, w =>
                    {
                        if(overallQuery == "All")
                            fileNames.Add(new SpurFile(System.IO.Path.GetFullPath(w)));
                        else if (w.Contains(overallQuery))
                        {
                            fileNames.Add(new SpurFile(System.IO.Path.GetFullPath(w)));
                        }
                    });
                    s.Stop();
                    BrowseBox.Text = fileNames.Count.ToString() + " files loading";
                    var token = new System.Threading.CancellationTokenSource();
                    CancellationToken ct = token.Token;
                    Task.Factory.StartNew(() =>
                    {
                        Stopwatch sw = new Stopwatch();
                        sw.Start();
                        while (true)
                        {
                            Parallel.ForEach(fileNames, f =>
                            {
                                overallBag.Add(_worker.ReadFiles(f));
                                this.Dispatcher.BeginInvoke(new Action(()=>
                                Label1.Content = "Files loaded: " + overallBag.Count()));
                            });
                            sw.Stop();
                            string time = sw.ElapsedMilliseconds.ToString();
                            supInfo = _worker.PopulateSupplierBox(overallBag);
                            this.Dispatcher.BeginInvoke(new Action(()=>
                                supplierNameBox.IsEnabled = true));
                            this.Dispatcher.BeginInvoke(new Action(()=>
                                supplierTypeBox.IsEnabled = true));
                            this.Dispatcher.BeginInvoke(new Action(()=>
                                supplierNameBox.ItemsSource = supInfo.supplierName));
                            this.Dispatcher.BeginInvoke(new Action(()=>
                                supplierTypeBox.ItemsSource = supInfo.supplierType));
                            complete = true;
                            break;
                        }
                        return overallBag;
                    }, ct);                                  
                    //while(!complete)
                    //{ }
                    //token.Cancel();

                }
            }
        }

        private void UpdateGrid(GridInfo gridInfo)
        {
            this.Grid.ItemsSource = gridInfo.gridList.ToArray();
        }

        private string SearchQuery(string storeQuery, string weekQuery)
        {
            string finalQuery = "";
            string store, week;
            store = storeQuery;
            week = weekQuery;
            if (storeQuery == "All" && weekQuery == "All")
                finalQuery = "All";
            if (storeQuery == "All" && weekQuery != "All")
                finalQuery = weekQuery;
            if (storeQuery != "All" && weekQuery == "All")
                finalQuery = storeQuery;
            if (storeQuery != "All" && week != "All")
                finalQuery = storeQuery + "_" + weekQuery + "_";
            
            return finalQuery;
        }

        
        
    }
}
