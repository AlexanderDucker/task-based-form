﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using System.Collections.Concurrent;

namespace Spur
{
    class Worker
    {
        public List<SpurFile> ReadFiles(SpurFile files)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            List<SpurFile> OverallList = new List<SpurFile>();
            var reader = new StreamReader(File.OpenRead((files.filePath)));
            List<SpurFile> tempList = new List<SpurFile>();
            while (!reader.EndOfStream)
            {
                try
                {
                    SpurFile tempFile = new SpurFile(files.filePath);
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    tempFile.fileName = System.IO.Path.GetFileName(files.filePath);
                    tempFile.supplierName = values[0];
                    tempFile.supplierType = values[1];
                    tempFile.orderCost = values[2];
                    tempList.Add(tempFile);
                }

                catch (Exception e)
                {
                    string error = e.ToString();
                    break;
                }
            }
            sw.Stop();
            return tempList;
        }

        public List<StoreCodes> LoadStoreCodes(FileInfo file)
        {
            List<StoreCodes> storeCodes = new List<StoreCodes>();
            var reader = new StreamReader(File.OpenRead(file.ToString()));
            while (!reader.EndOfStream)
            {
                StoreCodes tempFile = new StoreCodes();
                var line = reader.ReadLine();
                var values = line.Split(',');
                tempFile.storeCode = values[0];
                tempFile.storeName = values[1];
                storeCodes.Add(tempFile);
            }
            return storeCodes;
        }

        public List<String> PopulateStoreBox(List<StoreCodes> storeCodes)
        {
            List<String> storeNames = new List<string>();
            storeNames.Add("All");
            for (int i = 0; i < storeCodes.Count; i++)
            {
                storeNames.Add(storeCodes[i].storeName);
            }
            return storeNames;
        }

        public List<String> PopulateWeekBox()
        {
            List<String> weeks = new List<String>();
            weeks.Add("All");
            for (int i = 1; i <= 52; i++)
            {
                weeks.Add(i.ToString());
            }
            return weeks;
        }

        public SupplierInfo PopulateSupplierBox(ConcurrentBag<List<SpurFile>> file)
        {
            SupplierInfo supplierInfo = new SupplierInfo();
            foreach (List<SpurFile> l in file)
            {
                foreach (SpurFile f in l)
                {
                    if (!supplierInfo.supplierName.Contains(f.supplierName))
                        supplierInfo.supplierName.Add(f.supplierName);
                    if (!supplierInfo.supplierType.Contains(f.supplierType))
                        supplierInfo.supplierType.Add(f.supplierType);
                }
            }
            supplierInfo.supplierName.Add("All");
            supplierInfo.supplierType.Add("All");
            supplierInfo.supplierName.Add("N/A");
            supplierInfo.supplierType.Add("N/A");
            supplierInfo.supplierName.Sort();
            supplierInfo.supplierType.Sort();
            return supplierInfo;
        }

        public GridInfo CheckOrderCount(ConcurrentBag<List<SpurFile>> file, string supplierName, string supplierType)
        {
            double count = 0;
            GridInfo gridInfo = new GridInfo();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            if ((!supplierName.Equals("N/A") && !supplierName.Equals("All") && supplierType.Equals("N/A")))
            {
                foreach (List<SpurFile> l in file)
                {
                    foreach (SpurFile f in l)
                    {
                        if (f.supplierName == supplierName)
                        {
                            double cost = System.Convert.ToDouble(f.orderCost);
                            count += cost;
                            gridInfo.gridList.Add(f);
                        }
                    }
                }
            }

            else if(supplierName.Equals("N/A") && (!supplierType.Equals("N/A") && !supplierType.Equals("All")))
            {
                foreach (List<SpurFile> l in file)
                {
                    foreach (SpurFile f in l)
                    {
                        if (f.supplierType == supplierType)
                        {
                            double cost = System.Convert.ToDouble(f.orderCost);
                            count += cost;
                            gridInfo.gridList.Add(f);
                        }
                    }
                }
            }

            else if ((supplierName == ("All") && supplierType == ("N/A")))
            {
                foreach (List<SpurFile> l in file)
                {
                    foreach (SpurFile f in l)
                    {
                        double cost = System.Convert.ToDouble(f.orderCost);
                        count += cost;
                        gridInfo.gridList.Add(f);
                    }
                }
            }

            else if (supplierName == ("N/A") && supplierType == ("All"))
            {
                foreach (List<SpurFile> l in file)
                {
                    foreach (SpurFile f in l)
                    {
                        double cost = System.Convert.ToDouble(f.orderCost);
                        count += cost;
                        gridInfo.gridList.Add(f);
                    }
                }
            }

            else
            {
                gridInfo.cost = "Search Query Error";
                return gridInfo;
            }
            
            gridInfo.cost = "Total Cost: £" + count.ToString();
            sw.Stop();
            string time = sw.ElapsedMilliseconds.ToString();
            return gridInfo;
        }


    }
}
