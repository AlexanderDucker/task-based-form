﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spur
{
    class GridInfo
    {
        public ConcurrentBag<SpurFile> gridList { get; set; }
        public String cost { get; set; }

        public GridInfo()
        {
            gridList = new ConcurrentBag<SpurFile>();
        }
    }
}
